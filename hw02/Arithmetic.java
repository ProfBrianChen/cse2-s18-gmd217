// Garrett Deppert
// February 6, 2018
// CSE2 - hw02
// This program will record the total costs of various items bought from a store.

public class Arithmetic {
  // Main method required for every Java Program
  public static void main(String[] args) {
    
    int numPants = 3; // Number of pants bought from the store
    double pantsPrice = 34.98; // The cost per each pair of pants
    int numShirts = 2; // Number of sweatshirts bought from the store
    double shirtPrice = 24.99; // The cost per sweatshirt
    int numBelts = 1; // Number of belts bought from the store
    double beltCost = 33.99; // The cost per belt
    double paSalesTax = 0.06; // The tax rate in Pennsylvania
    
    double totalCostOfPants = numPants * pantsPrice; // Total cost of the pants
    double totalCostOfShirts = numShirts * shirtPrice; // Total cost of the sweatshirts
    double totalCostOfBelts = numBelts * beltCost; // Total cost of the belt
    
    double salesTaxOfPants = totalCostOfPants * paSalesTax; // Sales tax on the total cost of the belts
    double salesTaxOfPants1 = salesTaxOfPants * 100;
    int salesTaxOfPants2 = (int) salesTaxOfPants1;
    double salesTaxOfPants3 = salesTaxOfPants2 / 100.0;
    
    
    double salesTaxOfShirts = totalCostOfShirts * paSalesTax; // Sales tax on the total cost of the sweatshirts
    double salesTaxOfShirts1 = salesTaxOfShirts * 100;
    int salesTaxOfShirts2 = (int) salesTaxOfShirts1;
    double salesTaxOfShirts3 = salesTaxOfShirts2 / 100.0; 
    
    double salesTaxOfBelts = totalCostOfBelts * paSalesTax; // Sales tax on the total cost of the belt
    double salesTaxOfBelts1 = salesTaxOfBelts * 100;
    int salesTaxOfBelts2 = (int) salesTaxOfBelts1;
    double salesTaxOfBelts3 = salesTaxOfBelts2 / 100.0;
    
    double totalCostOfAllItems = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; // The total cost of all the items without sales tax
    
    double totalSalesTax = salesTaxOfPants3 + salesTaxOfShirts3 + salesTaxOfBelts3; // The total amount of sales tax
    
    double totalPaidForTransaction = totalCostOfAllItems + totalSalesTax; // The total cost of the whole transaction including sales tax
    
    System.out.println("The total cost of the pairs of pants bought from the store is " + totalCostOfPants + 
                       " dollars and the sales tax on the pairs of pants is " + salesTaxOfPants3 + " dollars. ");
    System.out.println("The total cost of the sweatshirts bought from the store is " + totalCostOfShirts + 
                       " dollars and the sales tax on the sweatshirts is " + salesTaxOfShirts3 + " dollars. ");
    System.out.println("The total cost of the belt bought from the store is " + totalCostOfBelts + 
                      " dollars and the sales tax on the belt is " + salesTaxOfBelts3 + " dollars. ");
    
    System.out.println("The total cost of the purchases before sales tax is " + totalCostOfAllItems + " dollars. ");
    System.out.println("The total amount of sales tax is " + totalSalesTax + " dollars. " );
    System.out.println("The total cost of the entire transaction including sales tax is " + totalPaidForTransaction + " dollars. ");
    
    
    
    
  } // end of main method
} // end of class