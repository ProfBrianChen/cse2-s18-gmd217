// Garrett Deppert
// March 2, 2018
// CSE2 - lab06
// This program will ask the user for an integer and print out the length of a twist

import java.util.Scanner; // importing scanner class
public class Twist {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // declaring instance of class
    
    boolean correct = false; 
    int length;
    while( !correct ) {
      System.out.print("Enter a positive integer: "); // Asks the user to enter a positive integer
      if( myScanner.hasNextInt() ) {
        length = myScanner.nextInt();
        if( length < 0 ) {
          System.out.print("Error you entered a negative integer. "); // Tests to see if the user entered a negative integer
        }
        else {
          correct = true;
        }
        // Print the top line of the twist out
        int i = 0;
        while( i < (length / 3 )) {
          System.out.print("\\ /");
        i++;
        }
        if( length % 3 == 1 ) {
          System.out.print("\\");
        }
        else if( length % 3 == 2 ) {
          System.out.print("\\ ");
        }
        // Print the middle line of the twist out
        i = 0;
        System.out.println();
        while( i < length / 3 ) {
          System.out.print(" X ");
        i++;
        }
        if( length % 3 == 1 ) {
          System.out.print(" ");
        }
        else if( length % 3 == 2 ) {
          System.out.print(" X");
        } 
        // Print the last line of the twist out
        i = 0;
        System.out.println();
        while( i < (length / 3 )) {
          System.out.print("/ \\");
        i++;
        }
        if( length % 3 == 1 ) {
          System.out.print("/");
        }
        else if( length % 3 == 2 ) {
          System.out.print("/ ");
        }
      }
      else{
        System.out.print("Error you did not enter an integer. "); // Tests to see if the user entered an integer
        String junkWord = myScanner.next();
      }
      
    }
    
    
    
  } // end of main method
} // end of class
