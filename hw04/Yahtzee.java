// Garrett Deppert
// February 20, 2018
// CSE2 - hw04
// This program will perform a random roll of the dice and score the roll
// In a game of Yahtzee

import java.util.Scanner; // importing the scanner class

public class Yahtzee {
  // Every Java program requires a main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // declaring instance of Scanner class 
    
    // When prompted enter 1 for a randomly generated number or enter 2 to input your own number
    System.out.print("Do you want a randomly generated number [1] or input your own? [2]"); // Asks the user if they want a random number or input their own
    int method = myScanner.nextInt();
    
    int inputNumber = 10000 + (int) (Math.random()*10000); // This will generate a random 5 digit number
    
    if( method == 2 ) {
      System.out.print("Enter a 5 digit number: "); // The user will enter a five digit number indicating 5 rolls of the dice
      inputNumber = myScanner.nextInt(); // Allows the user to input their own 5 digit number
      
      // 
      int integer = inputNumber / 10000;
      if ( integer == 0 ) {
        System.out.println("The number provided was in error."); // Will tell user the number is invalid
        return; // Will shut down program due to an error in number
      }
      
    }
    else {
      
      System.out.println(inputNumber); // Prints out a random 5 digit number
    }
    
    // Generate a  five random digits 
    int randomDigit1 = inputNumber / 10000; 
    int remainder1 = inputNumber % 10000;
    //System.out.println(randomDigit1 + " " + remainder1); 
    
    int randomDigit2 = remainder1 / 1000;
    int remainder2 = remainder1 % 1000;
    //System.out.println(randomDigit2 + " " + remainder2);
    
    int randomDigit3 = remainder2 / 100;
    int remainder3 = remainder2 % 100;
    //System.out.println(randomDigit3 + " " + remainder3);
    
    int randomDigit4 = remainder3 / 10;
    int remainder4 = remainder3 % 10;
    //System.out.println(randomDigit4 + " " + remainder4);
    
    int randomDigit5 = remainder4;
    //System.out.println(randomDigit5 + " " + remainder4);
    
    // Commands to show that the number inputted or generated is an error and the program cannot progress
    if( randomDigit1 == 0 || randomDigit2 == 0 || randomDigit3 == 0 || randomDigit4 == 0 || randomDigit5 == 0) {
      System.out.println("The number provided was in error.");
      return; // Shuts down the program due to an error
    }
    
    if( randomDigit1 > 0 && randomDigit1 > 6) {
      System.out.println("The number provided was in error.");
      return; // Shuts down the program due to an error
    }
    else if( randomDigit2 > 0 && randomDigit2 > 6) {
      System.out.println("The number provided was in error.");
      return; // Shuts down the program due to an error
    }
    else if( randomDigit3 > 0 && randomDigit3 > 6) {
      System.out.println("The number provided was in error.");
      return; // Shuts down the program due to an error
    }
    else if( randomDigit4 > 0 && randomDigit4 > 6) {
      System.out.println("The number provided was in error.");
      return; // Shuts down the program due to an error
    }
    else if( randomDigit5 > 0 && randomDigit5 > 6) {
      System.out.println("The number provided was in error.");
      return; // Shuts down the program due to an error
    }
    else{
      //System.out.println(randomDigit1 + " " + randomDigit2 + " " + randomDigit3 + " " + randomDigit4 + " " + randomDigit5);
    }
    
   // upper section score
   int aces = 0; 
   int twos = 0;
   int threes = 0;
   int fours = 0;
   int fives = 0;
   int sixes = 0;
    
   // Will add all the numbers generated or inputted in random digit 1
   switch ( randomDigit1 ) {
       
     case 1: 
     aces += 1;
     break;
     
     case 2:
     twos += 1;
     break;
     
     case 3:
     threes += 1;
     break;
       
     case 4:
     fours += 1;
     break;
       
     case 5:
     fives += 1;
     break;
     
     case 6:
     sixes += 1;
     break;
       
   }
   
    // Will add numbers the twos generated or inputted in random digit 2
    switch ( randomDigit2 ) {
       
     case 1: 
     aces += 1;
     break;
     
     case 2:
     twos += 1;
     break;
     
     case 3:
     threes += 1;
     break;
       
     case 4:
     fours += 1;
     break;
       
     case 5:
     fives += 1;
     break;
     
     case 6:
     sixes += 1;
     break;
       
   }
   
   // Will add all the numbers generated or inputted in random digit 3
   switch ( randomDigit3 ) {
       
     case 1: 
     aces += 1;
     break;
     
     case 2:
     twos += 1;
     break;
     
     case 3:
     threes += 1;
     break;
       
     case 4:
     fours += 1;
     break;
       
     case 5:
     fives += 1;
     break;
     
     case 6:
     sixes += 1;
     break;
       
   }
   
   // Will add all the numbers generated or inputted in random digit 4
   switch ( randomDigit4 ) {
       
     case 1: 
     aces += 1;
     break;
     
     case 2:
     twos += 1;
     break;
     
     case 3:
     threes += 1;
     break;
       
     case 4:
     fours += 1;
     break;
       
     case 5:
     fives += 1;
     break;
     
     case 6:
     sixes += 1;
     break;
       
   }
   
   // Will add all the numbers generated or inputted in random digit 5
   switch ( randomDigit5 ) {
       
     case 1: 
     aces += 1;
     break;
     
     case 2:
     twos += 1;
     break;
     
     case 3:
     threes += 1;
     break;
       
     case 4:
     fours += 1;
     break;
       
     case 5:
     fives += 1;
     break;
     
     case 6:
     sixes += 1;
     break;
       
   }
   
   int upperSectionScoreRoll = aces * 1 + twos * 2 + threes * 3 + fours * 4 + fives * 5 + sixes * 6;
    
   // System.out.println(aces + " " + twos + " " + threes + " " + fours + " " + fives + " " + sixes + " " + upperSectionScoreRoll);
    
   if ( upperSectionScoreRoll > 63) {
     upperSectionScoreRoll += 35;
     //System.out.println(upperSectionScoreRoll);
   }
   else {
     int upperSectionScore = upperSectionScoreRoll; 
    // System.out.println(upperSectionScore);
   }
   
   
   // lower section score
    
   // large straight score
   int largeStraight = 0;
   
   if( aces > 1 || twos > 1 || threes > 1 || fours > 1 || fives > 1 || sixes > 1 ) {
     largeStraight = 0;
   }
   else if( randomDigit4 > randomDigit3 && randomDigit3 > randomDigit2 && randomDigit2 > randomDigit1 ) {
      largeStraight = 40;
   }
   else {
     largeStraight = 0;
    
   }
   //System.out.println(largeStraight);
    
   // small straight score
   int smallStraight = 0;
   if( largeStraight == 0 ) {
     if( randomDigit3 > randomDigit2 && randomDigit2 > randomDigit1 ) {
       smallStraight = 30; 
     }
   }
   else {
    smallStraight = 0;
    
   }
   //System.out.println(smallStraight);
 
   // Yahtzee score 
   int yahtzee = 0;
    
   if( aces == 5 || twos == 5 || threes == 5 || fours == 5 || fives == 5 || sixes == 6 ) {
     yahtzee = 50;
   }
   else {
     yahtzee = 0;
   }
   //System.out.println(yahtzee);
    
   // Full house score
   int fullHouse = 0;
   if( aces == 3 && twos == 2 || threes == 2 || fours == 2 || fives == 2 || sixes == 2) {
     fullHouse = 25;
   }
   else if( twos == 3 && aces == 2 || threes == 2 || fours == 2 || fives == 2 || sixes == 2 ) {
     fullHouse = 25;
   }
   else if( threes == 3 && twos == 2 || aces == 2 || fours == 2 || fives == 2 || sixes == 2 ) {
     fullHouse = 25;
   }
   else if( fours == 3 && twos == 2 || threes == 2 || aces == 2 || fives == 2 || sixes == 2 ) {
     fullHouse = 25;
   }
   else if( fives == 3 && twos == 2 || threes == 2 || fours == 2 || aces == 2 || sixes == 2 ) {
     fullHouse = 25;
   }
   else if( sixes == 3 && twos == 2 || threes == 2 || fours == 2 || fives == 2 || aces == 2 ) {
     fullHouse = 25;
   }
   else {
     fullHouse = 0;
   }  
 
   
   //System.out.println(fullHouse); 
    
   // 3 of a kind score
   int threeOfKind = 0;
   int sumOfDice = randomDigit1 + randomDigit2 + randomDigit3 + randomDigit4 + randomDigit5;
   if( fullHouse == 0 ) {
      if( aces == 3 || twos == 3 || threes == 3 || fours == 3 || fives == 3 || sixes == 3 ) {
        threeOfKind = sumOfDice; 
      }
      else {
        threeOfKind = 0;
      }
   }
  //System.out.println(threeOfKind);
    
   // 4 of kind score 
   int fourOfKind = 0;
   if( aces == 4 || twos == 4 || threes == 4 || fours == 4 || fives == 4|| sixes == 4 ) {
     fourOfKind = sumOfDice; 
   }
   else {
     fourOfKind = 0;
   }
   //System.out.println(fourOfKind);
    
   
   
   // Chance score
   int chance = 0;
   if( smallStraight == 0 && largeStraight == 0 && yahtzee == 0 && fullHouse == 0 && threeOfKind == 0 && fourOfKind == 0 ) {
     chance = randomDigit1 + randomDigit2 + randomDigit3 + randomDigit4 + randomDigit5;
   }
   else {
     chance = 0;
   }
   //System.out.println(chance);
    
   // Add up the totals 
   int upperSectionTotal = upperSectionScoreRoll;
    
   int lowerSectionTotal = chance + yahtzee + threeOfKind + fourOfKind + fullHouse + largeStraight + smallStraight;
    
   int grandTotal = upperSectionTotal + lowerSectionTotal;
    
   System.out.println("The uppersection total is: " + upperSectionTotal + ". ");
   System.out.println("The lowersection total is: " + lowerSectionTotal + ". ");
   System.out.println("The grand total is: " + grandTotal + ". ");
   
     
   
  
  
  } // end of main method
} // end of class