// Garrett Deppert
// March 9, 2018
// CSE2 - lab06
// This program will display secret message x

import java.util.Scanner; // importing scanner class
public class encrypted_x {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // declaring instance of the scanner class
    boolean correct = false;
    int input;
    while( !correct ) {
      System.out.print("Enter a integer between 1 and 100: ");
      if( myScanner.hasNextInt() ) {
        input = myScanner.nextInt(); // this will be the size of the square measured by the number of stars
        if( input < 1 || input > 100 ) {
           System.out.print("Error: you entered an invalid integer. "); // tests to see if the integer the user entered is between 1-100
        }
        
        else {
          correct = true; 
          for( int i = 0; i < input; i++ ) {
            for( int j = 0; j < input; j++ ) {
              if( input - i - 1 == j ) {
                System.out.print("  ");
              } 
              else if( j == i ) {
                System.out.print("  ");
              }
              else {
                System.out.print("*");
              }
              
            }
            System.out.println();
         }
    
        }
       
      }
      
     else {
        System.out.print("Error: you did not enter an integer. "); // tests to see if the user entered an integer
        String junkWord = myScanner.next();
      }
    }
    

    
   
    
   
    
  } // end of main method
} // end of class