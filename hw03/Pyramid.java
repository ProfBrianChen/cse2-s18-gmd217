// Garrett Deppert
// February 13, 2018
// CSE2 - hw03
// This program will display the dimensions of a pyramid
// It will also give the volume of the pyramid

import java.util.Scanner; // importing the class Scanner
public class Pyramid {
  // Main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // declaring instance of Scanner class
    
    System.out.print("The square side of the pyramid is: "); // The length of the square side of the pyramid
    double squareSideOfPyramid = myScanner.nextDouble();
    
    System.out.print("The height of the pyramid is: "); // The height of the pyramid
    double heightOfPyramid = myScanner.nextDouble();
    
    double volumeOfPyramid = squareSideOfPyramid * squareSideOfPyramid * heightOfPyramid / 3; // This will calculate the volume inside the pyramid
    
    System.out.println("The volume inside the pyramid is: " + volumeOfPyramid);
    
  } // end of main method
} // end of class