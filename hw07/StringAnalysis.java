// Garrett Deppert
// March 27, 2018
// CSE2 - hw07
// This program will process a string of characters and determine if they are letters

import java.util.Scanner; // importing the scanner class

public class StringAnalysis {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // declaring scanner class
    
    System.out.print("Enter a string: ");
    boolean newString = stringAnalysis( myScanner.next() );
    //String analyze = stringAnalysis( newString );
    
    /*System.out.print("Enter an integer: ");
    int characterNumb = myScanner.nextInt();
    System.out.print("Enter a string: ");
    String newString1 = myScanner.next();*/
    
  } // end of main method
  
  
  public static boolean stringAnalysis( String characters ) {
      Scanner myScanner = new Scanner(System.in);
      //System.out.print("Enter a string: ");
      String newString = myScanner.next();
      for( int i = 0; i < newString.length(); i++ ) {
        if( Character.isLetter( newString.charAt(i) ) ) {
          return true;
        }
        else {
          return false;
        }
      }
      return true;
  }
    
  public static boolean stringAnalysis( String characters  , int numberOfCharacters ) {
      Scanner myScanner = new Scanner(System.in);
      String newString = myScanner.next();
      for( int i = 0; i < numberOfCharacters; i++ ) {
        
        if( Character.isLetter( newString.charAt(i) ) ) {
          return true;
        }
        else {
          return false;
        }
      }
      return true;
      
  }
  
} // end of class