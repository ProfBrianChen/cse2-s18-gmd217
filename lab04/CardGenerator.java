// Garrett Deppert
// February 16, 2018
// CSE2- lab04
// This program will display a random card from a 52 card deck

public class CardGenerator {
  // Every Java program requires a class
  public static void main(String[] args) {
    
    int num = (int) (Math.random()*52); // Display a random number
    
    int cardNumber = num % 13; // Take the random number and divide it by 13 and take the remainder
    
    int cardSuit = cardNumber / 4; // This will give the suit of the card
    
   
    // Determines the identity of the card
    switch( cardNumber ) {
      case 0:
      System.out.print("You picked the Ace of ");
      break;
      
      case 1:
      System.out.print("You picked the 2 of ");
      break;
        
      case 2:
      System.out.print("You picked the 3 of ");
      break;
      
      case 3:
      System.out.print("You picked the 4 of ");
      break;
        
      case 4:
      System.out.print("You picked the 5 of ");
      break;
      
      case 5:
      System.out.print("You picked the 6 of ");
      break;
        
      case 6:
      System.out.print("You picked the 7 of ");
      break;
      
      case 7:
      System.out.print("You picked the 8 of ");
      break;
        
      case 8:
      System.out.print("You picked the 9 of ");
      break;
        
      case 9:
      System.out.print("You picked the 10 of ");
      break;
      
      case 10:
      System.out.print("You picked the Jack of ");
      break;
        
      case 11:
      System.out.print("You picked the Queen of ");
      break;
      
      case 12:
      System.out.print("You picked the King of ");
      break;
      }
    
    // Determines what the suit of the card will be
    if( cardSuit == 0 ) {
      System.out.print("Diamonds.");
    }
    else if( cardSuit == 1 ) {
      System.out.print("Clubs.");
    }
    else if( cardSuit == 2 ) {
      System.out.print("Hearts.");
    }
    else if( cardSuit == 3 ) {
      System.out.print("Spades.");
    }
    
  } // end of main method
} // end of class