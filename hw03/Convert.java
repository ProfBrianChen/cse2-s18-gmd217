// Garrett Deppert
// February 13, 2018
// CSE2 - hw03
// This program will display the acres of land affect by hurrican preciptation
// It will also display the how inches of rain were dropped on average

import java.util.Scanner; // import the class Scanner
public class Convert {
   // Main method requried for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // declaring instance of Scanner class  
    
    System.out.print("Enter the affected area in acres: "); // The amount of acres affected by the rainfall
    double areaAffected = myScanner.nextDouble();
    
    System.out.print("Enter the rainfall in the affected area: "); // The total amount of rainfall by the hurricane
    double rainfall = myScanner.nextDouble();
    
    
    int squareFeetInAcres = 43560; // There are 43560 square feet in 1 acre
    int inchesInAFoot = 12; // There are 12 inches in 1 foot
    int feetInAMile = 5280; // There are 5280 feet in a mile
    
    double feetInAMileCubed = Math.pow(5280, 3); // Computes 5280 Cubed
    
    double acresToSquareFeet = areaAffected * squareFeetInAcres; // Converts acres to square feet 
    double rainToFeet = rainfall / 12; // Converts inches of rain to feet
    double volumeCubicFeet = rainToFeet * acresToSquareFeet; // Volume in cubic feet 
    
    
    double rainfallInCubicMiles = volumeCubicFeet / feetInAMileCubed; // Converts the quantity of rainfall into cubic miles
  
    
    System.out.println("The amount of rain in cubic miles is: " + rainfallInCubicMiles);

    
    
  
  } // end of main method
} // end of class
 
    
      
    
    
