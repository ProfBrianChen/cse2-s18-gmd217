// Garrett Deppert
// February 9, 2018
// CSE2 - lab03
// This program will display the amount of the total check and how much each person has to pay

import java.util.Scanner; // importing Scanner class 
public class Check {
  // main method required for every Java program
  public static void main(String [] args) {
    Scanner myScanner = new Scanner(System.in); // declaring instance of Scanner class
    
    System.out.print("Enter the original cost of the check: ");
    
    double checkCost = myScanner.nextDouble(); // How much the total check will cost
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number: "); // How much the tip is going to be paid
    
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; // convert the percentage into decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); // How many people went to the dinner
    int numPeople = myScanner.nextInt(); 
    
    double totalCost;
    double costPerPerson;
    int dollars;
    int dimes;
    int pennies;
    
    totalCost = checkCost * (1 + tipPercent); // the total cost of the check
    costPerPerson = totalCost / numPeople; // how much each person pays
    dollars = (int) costPerPerson; // the amount of dollars
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
    
    System.out.println("Each person in the group owes $ " + dollars + " . " + dimes + pennies);
    
    
  } // end of main method
} // end of class