// Garrett Deppert
// March 20, 2018
// CSE2 - hw06
// This program will display an argyle pattern

import java.util.Scanner; // importing the scanner class

public class Argyle {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // declaring instance of the scanner class
    
    boolean correct = false;
    int width = -1;
    int height = -1;
    int widthOfArgyleDiamond = -1;
    int widthOfArgyleStripe = -1;
    // Tests to see if the user entered a positive integer
    while( !correct ) {
      System.out.print("Enter a positive integer for the width of the display window in characters: ");
      if( myScanner.hasNextInt() ) {
        width = myScanner.nextInt();
        if( width < 0 ) {
          System.out.println("Error: you entered a negative number");
        }
        else {
          correct = true;
        }
      }
      else {
        String junkWord = myScanner.next();
        System.out.println("Error: you did not enter an integer.");
      }
    }
    // Tests to see if the user entered a positive integer
    correct = false;
    while( !correct ) {
      System.out.print("Enter a positive integer for the height of the display window in characters: ");
      if( myScanner.hasNextInt() ) {
        height = myScanner.nextInt();
        if( height < 0 ) {
          System.out.println("Error: you entered a negative number");
        }
        else {
          correct = true;
        }
      }
      else {
        String junkWord = myScanner.next();
        System.out.println("Error: you did not enter an integer.");
      }
    }
    // Tests to see if the user entered a positive integer
    correct = false;
    while( !correct ) {
      System.out.print("Enter a positive integer for the width of the argyle diamonds: ");
      if( myScanner.hasNextInt() ) {
        widthOfArgyleDiamond = myScanner.nextInt();
        if( widthOfArgyleDiamond < 0 ) {
          System.out.println("Error: you entered a negative number");
        }
        else {
          correct = true;
        }
      }
      else {
        String junkWord = myScanner.next();
        System.out.println("Error: you did not enter an integer.");
      }
    }
    // Tests to see if the user entereed the correct width for the argyle center strip
    correct = false;
    while( !correct ) {
      System.out.print("Enter an odd positive integer for the width of the argyle center strip: ");
      if( myScanner.hasNextInt() ) {
        widthOfArgyleStripe = myScanner.nextInt();
        if( widthOfArgyleStripe < 0 ) {
          System.out.println("Error: you entered a negative number");
        }
        else if( widthOfArgyleStripe % 2 == 0 ) {
          System.out.println("Error: you entered an even number");
        }
        else if( widthOfArgyleStripe > (widthOfArgyleDiamond / 2) ) {
          System.out.println("Error: you entered a number greater than half of the argyle size");
        }
        else {
          correct = true;
        }
      }
      else {
        String junkWord = myScanner.next();
        System.out.println("Error: you did not enter an integer.");
      }
    }
    // Asks the user to enter a character of his or her choice
    System.out.print("Enter a first character for the pattern fill: ");
    String charOne = myScanner.next();
    char result = charOne.charAt(0); 
    
    System.out.print("Enter a second character for the pattern fill: ");
    String charTwo = myScanner.next();
    char result2 = charTwo.charAt(0);
    
    System.out.print("Enter a third character for the pattern fill: ");
    String charThree = myScanner.next();
    char result3 = charThree.charAt(0);
    
    char dots = result; 
    char plus = result2;
    char percents = result3;
    int argyleDiamond = widthOfArgyleDiamond;
    int numberOfPlusSigns = 0;
    int x = 0;
    int y = 0;
    int z = 0;
    int diamondMiddle;
    int counter = widthOfArgyleDiamond ;
    /*if( height % 2 == 0 ) {
      diamondMiddle = height / 2 + 1;
    }
    else {
      diamondMiddle = height / 2 + 2;
    }*/
    int plusMin = widthOfArgyleDiamond;
    int plusMax = widthOfArgyleDiamond;
    for( int i = 0; i < height + 2; i++ ) {
      for( int j = 0; j < widthOfArgyleDiamond - 1 ; j++ ) {
        if( j ==  i ) {
          System.out.print(percents);
        }
        else if( j >= plusMin && i != 0 ) {
          System.out.print(plus);
        }
        else {
          System.out.print(dots);
        } 
       }
     
      for( int k = widthOfArgyleDiamond; k < widthOfArgyleDiamond * 2; k++) {
          if( k == widthOfArgyleDiamond * 2 - i ) {
            System.out.print(percents);
          }
          else if( k <= plusMax && i != 0) { 

            System.out.print(plus);
          }
  
        else {
          System.out.print(dots);
        }
      }
      /*for( int l = height + 2; l < widthOfArgyleDiamond - 1; l++ ) {
        if( l == widthOfArgyleDiamond - i ) {
          System.out.print(percents);
        }
        else if( l >= plusMin && i != 0 ) {
          System.out.print(plus);
        }
      }*/
      System.out.println();
      plusMin--;
      plusMax++;
    }
    
     
  } // end of main method
} // end of class

    
    