// Garrett Deppert
// March 27, 2018
// CSE2 - hw07
// This program will ask the user to enter the correct shape 
// and then dipslay the area with the given demensions

import java.util.Scanner; // importing the Scanner class
public class Area {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
   
    String s1 = "triangle";
    String s2 = "circle";
    String s3 = "rectangle";
    boolean correct = false;
    String shape = " ";
    while( !correct ) {
      System.out.print("Enter a shape: ");
      shape = myScanner.next();
      if( !shape.equals(s1) && !shape.equals(s2) && !shape.equals(s3) ) {   
        System.out.println("Error: Please enter a triangle, circle, or rectangle.");
      }
      if( shape.equals(s1) || shape.equals(s2) || shape.equals(s3) ) {
        correct = true;
      }
     
   }
   // Will use the inputs the user gives and display the area of a triangle
   if( shape.equals(s1) ) {
     System.out.print("Enter the base of the triangle: ");
     double base = myScanner.nextDouble();
     System.out.print("Enter the height of the triangle: ");
     double height = myScanner.nextDouble();
     double area = triangleArea( base , height );
     System.out.println("The area of the triangle is: " + area);
   }
   // Will use the inputs the user gives and display the area of a rectangle
   else if( shape.equals(s3) ) {
     System.out.print("Enter the length of the rectangle: ");
     double length = myScanner.nextDouble();
     System.out.print("Enter the width of the rectangle: ");
     double width = myScanner.nextDouble();
     double area = rectangleArea( length , width );
     System.out.println("The area of the rectangle is: " + area);
   }
   // Will use the inputs the user gives and display the area of a cricle
   else if( shape.equals(s2) ) {
     System.out.print("Enter the raidus of the cricle: ");
     double radius = myScanner.nextDouble();
     double area = circleArea( radius );
     System.out.println("The area of the cirlce is: " + area);
   }
    
    
    
    
  } // end of main method
  
  // This is the triangle method and will make sure the user enters a double for the base and height
  // Will calculate and return the area of the triangle
  public static double triangleArea( double base , double height ) {
      return base * height * .5;
  
  }

  // This is the rectangle method and will make sure the user enters a double for the length and width 
  // Will calculate and return the area of the rectangle
  public static double rectangleArea( double length , double width ) {
      return length * width;
  }

  // This the circle method and will make sure the user enters a double for the raidus
  // Will calculate and return the area of the circle
  public static double circleArea( double radius ) {
      return 3.14 * radius * radius;
  }
  
  
  
} // end of class



