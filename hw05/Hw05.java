// Garrett Deppert
// March 6, 2018
// CSE2 - hw06
// This program will talk about a class a student is currently taking

import java.util.Scanner; // Importing the scanner class

public class Hw05 {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // Declaring instance of scanner class
    Scanner myScanner2 = new Scanner(System.in); // Different declarations of the scanner class
    Scanner myScanner3 = new Scanner(System.in); // allows me to use .nextLine() after .nextInt
    Scanner myScanner4 = new Scanner(System.in);
    /* This will display the course name
    and will ask the user to enter another input 
    if the user enters the wrong type of input the
    first time. */ 
    System.out.print("Enter course name: ");
    while( !myScanner.hasNextLine() ) {
      String junkNumber = myScanner.next();
      System.out.println("Error: Wrong type ");
      System.out.print("Enter course name: "); 
    }
    String courseName = myScanner.nextLine();
    
    /* This will display the department name
    and will ask the user to enter another input 
    if the user enters the wrong type of input the
    first time. */
    System.out.print("Enter department name: ");
    while( !myScanner.hasNextLine() ) {
      String junkNumber = myScanner.next();
      System.out.println("Error: Wrong type ");
      System.out.print("Enter department name: ");
    }
    String departmentName = myScanner.nextLine();
    
    /*This will display the course number
    and will ask the user to enter another input 
    if the user enters the wrong type of input the
    first time. The user must enter an integer for 
    the program to run. */
    System.out.print("Enter course number: ");
    while( !myScanner.hasNextInt() ) {
      String junkNumber = myScanner.next();
      System.out.println("Error: Wrong type ");
      System.out.print("Enter course number: ");    
    } 
    int courseNumber = myScanner.nextInt(); 
   
    /* This will display the amount of times the class meets 
    per week and will ask the user to enter another input 
    if the user enters the wrong type of input the
    first time. The user must enter an integer for the 
    program to run. */
    System.out.print("Enter the number of times per week the class meets: ");
    while( !myScanner.hasNextInt() ) {
      String junkNumber = myScanner.next();
      System.out.println("Error: Wrong type ");
      System.out.print("Enter the number of times per week the class meets: ");   
    }
    int timesPerWeek = myScanner.nextInt();
    
    /* This will display the time the class starts
    and will ask the user to enter another input 
    if the user enters the wrong type of input the
    first time. I used a string variable type to allow 
    for the user to input a semicolo. Ex: 12:10. */
    System.out.print("Enter the time the class starts: ");
    while( !myScanner2.hasNextLine() ) {
      String junkNumber = myScanner2.next();
      System.out.println("Error: Wrong type ");
      System.out.print("Enter the time the class starts: ");
    }
    String startOfClass = myScanner2.nextLine();
    
    /* This will display the instructor of the class
    and will ask the user to enter another input 
    if the user enters the wrong type of input the
    first time. */
    System.out.print("Enter the instructor of the class: ");
    while( !myScanner3.hasNext() ) {
      String junkNumber = myScanner3.next();
      System.out.println("Error: Wrong type ");
      System.out.print("Enter the instructor of the class: ");   
    }
    String instructorOfClass = myScanner3.nextLine();
    
    /* This will display the number of students in the class
    and will ask the user to enter another input 
    if the user enters the wrong type of input the
    first time. The user must input an integer for the program 
    to continue. */
    System.out.print("Enter the number of students in the class: ");
    while( !myScanner4.hasNextInt() ) {
      String junkNumber = myScanner4.next();
      System.out.println("Error: Wrong type ");
      System.out.print("Enter the number of students in the class: ");   
    }
    int numberStudents = myScanner4.nextInt();
      
  } // end of main method
} // end of class