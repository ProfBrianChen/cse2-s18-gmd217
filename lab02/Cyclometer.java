// Garrett Deppert
// February 2, 2018
// CSE 2 - lab02
// The program will record the time and counts of the two trips

public class Cyclometer {
  // Main method required for every Java program
  public static void main(String[] args) {
    int secsTrip1 = 480; //number of seconds for trip 1 = 480
    int secsTrip2 = 3220; //number of seconds for trip 2 = 3220
    int countsTrip1 = 1561; //number of counts for trip 1 = 1561
    int countsTrip2 = 9037; //number of counts for trip 2 = 9037
    
    double wheelDiameter = 27.0; //The diameter of the wheel
    double PI = 3.14159; //The constant PI
    int feetPerMile = 5280; //There are 5280 feet per mile
    int inchesPerFoot = 12; // There are 12 inches in a foot
    double secondsPerMinute = 60; //There are 60 seconds in a minute
    double distanceTrip1 = countsTrip1 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //Gives the total miles of trip 1 in miles
    double distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //Gives the total miles of trip 2 in miles
    double totalDistance = distanceTrip1 + distanceTrip2; //The number of miles traveled between both trips
      
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts. ");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts. ");
    System.out.println("Trip 1 was " + distanceTrip1 + " miles. ");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles. ");
    System.out.println("The total distance was " + totalDistance + " miles. ");
    
    
    
    
  
  
  } //end of main method
} //end of class