// Garrett Deppert
// February 6, 2018
// CSE2 - hw01
// This program will welcome the class and give a short autobiography statement about me
public class WelcomeClass {
  // Main method required for every Java program
  public static void main (String[] args) {
   
    // Welcoming the class
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\"); 
    System.out.println("<-G--M--D--2--1--7->"); // Lehigh network ID
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    
    // Short autobiography statement
    System.out.println("My name is Garrett Deppert. I am 18 years old and I am a senior at Saucon Valley High School. I will attend Lehigh next fall to study CSE.");
  } // end of main method
} // end of class